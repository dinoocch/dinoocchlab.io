<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<title>Paxos Variants and Extensions</title>
	<meta name="description" content="Overview of Paxos Variants and Extensions">
	<meta name="author" content="Dino Occhialini">

	<link rel="stylesheet" href="css/reveal.css">
	<link rel="stylesheet" href="css/theme/black.css">

	<!-- Theme used for syntax highlighting of code -->
	<link rel="stylesheet" href="lib/css/zenburn.css">

	<style>
		.reveal section img {
			background: none;
			border: none;
			box-shadow: none;
		}
	</style>

	<!-- Printing and PDF exports -->
	<script>
		var link = document.createElement('link');
		link.rel = 'stylesheet';
		link.type = 'text/css';
		link.href = window.location.search.match(/print-pdf/gi) ? 'css/print/pdf.css' : 'css/print/paper.css';
		document.getElementsByTagName('head')[0].appendChild(link);
	</script>
</head>

<body>
	<div class="reveal">
		<div class="slides">
			<section>
				<h3>Situational Paxos Variants</h3>
				<h5>Dino Occhialini</h5>
			</section>

			<section>
				<h3>Importance</h3>
				<ul>
					<li>Consensus is important</li>
					<li>Paxos is important (and practical)</li>
					<li>Different variations are optimal in different situations</li>
					<li>Understanding extensions to the algorithm can help implementors craft a solution that better fits their problem domain</li>
				</ul>
			</section>

			<section>
				<h3>Potential Areas of Improvement</h3>
				<ul>
					<li>Processor Overhead</li>
					<li>Message Delay</li>
					<li>Leader Bottlenecks</li>
					<li>Others?</li>
				</ul>
			</section>

			<section>
				<section>
					<h3>Reducing Processor Overhead</h3>
				</section>

				<section>
					<h3>Why is this important</h3>
					<ul>
						<li>Consensus requires <em>2f+1</em> processes</li>
						<li>How many processes are failed at a time?</li>
						<li>Consider system with very low failure rate but high failure tolerance requirements</li>
						<li>We can't reduce the number of processes</li>
						<li>Can we make them cheaper though?</li>
					</ul>
				</section>

				<section>
					<h3>What it means to be cheap</h3>
					<ul>
						<li>Processor power</li>
						<li>Disk/Memory</li>
						<li>Network</li>
					</ul>
				</section>

				<section>
					<h3>Consider a system designed to support 2 failures (5 nodes)</h3>
					<img class="plain" style="width:50%" src="images/proc1.svg"></img>
					<div>
						<span>Every process is "expensive"</span>
					</div>
				</section>

				<section>
					<h3>What happens if we make some cheaper?</h3>
					<img class="plain" style="width:50%" src="images/proc2.svg"></img>
					<div>
						<span>3 Expensive, 2 Cheap</span>
					</div>
				</section>

				<section>
					<h3>Making a request</h3>
					<img class="plain" style="width:50%" src="images/cheap1.svg"></img>
					<div>
						<span>3 Expensive, 2 Cheap</span>
					</div>
				</section>

				<section>
					<h3>Receiving a response</h3>
					<img class="plain" style="width:50%" src="images/cheap2.svg"></img>
					<div>
						<span>3 Expensive, 2 Cheap</span>
					</div>
				</section>

				<section>
					<h3>Observations</h3>
					<ul>
						<li>Expensive/Fast processes probably respond first</li>
						<li>As long as no nodes fail, the system stays fast</li>
						<li>No impact to fault tolerance</li>
						<li style="color:#ff665b">Doesn't reduce network overhead</li>
						<li style="color:#ff665b">Doesn't reduce disk requirements</li>
						<li style="color:#ff665b">System slows down during failure</li>
					</ul>
				</section>

				<section>
					<h3>"Distinguished Quorum"</h3>
					<img class="plain" style="width:50%" src="images/cheap3.svg"></img>
					<div>
						<span>3 Expensive, 2 Cheap</span>
					</div>
				</section>

				<section>
					<h3>"Distinguished Quorum"</h3>
					<img class="plain" style="width:50%" src="images/cheap4.svg"></img>
					<div>
						<span>Need to adjust quorum on failure</span>
					</div>
				</section>

				<section>
					<h3>Observations</h3>
					<ul>
						<li>Reduced Network Communications</li>
						<li style="color:#ff665b">Requires a failure detector</li>
						<li style="color:#ff665b">System slows down during failure</li>
						<li style="color:#ff665b">Doesn't reduce disk requirements</li>
					</ul>
				</section>

				<section>
					<h3>Cheap Paxos[1]</h3>
					<ul>
						<li>Main and Aux nodes</li>
						<li>Main nodes participate in computation, aux nodes deal with failure</li>
					</ul>
					<img class="plain" style="width:50%" src="images/cheap5.svg"></img>
				</section>

				<section>
					<h3>Cheap Paxos[1] details</h3>
					<ul>
						<li>On failure, one or more aux processes allow main processes to remove a process from the view</li>
						<li>Also handle in-progress requests</li>
						<li>When nodes are repaired/added, main processors can add to view by themselves</li>
						<li style="color:#56ff8e">Reduces disk, processing, and network requirements</li>
						<li style="color:#ff665b">Requires a failure detector</li>
						<li style="color:#ff665b">System delays until failure is detected</li>
					</ul>
				</section>
			</section>

			<section>
				<section>
					<h3>Reducing (normal-case) delay</h3>
				</section>
				<section>
					<h3>Flow of messages in Paxos</h3>
					<pre><code>Leader &rarr; Acceptors &rarr; Learners (2 delays)</code></pre>
					<span>What if Leader is not proposer?</span>
					<pre><code>Proposer &rarr; Leader &rarr; Acceptors &rarr; Learners (3 delays)</code></pre>
				</section>
				<section>
					<h3>Enter Fast Paxos[2]</h3>
					<ul>
						<li><em>Idea: Send proposals directly to Acceptors</em></li>
						<li>What if multiple requests are sent concurrently?</li>
						<li>Divide ballot numbers into <em>fast</em> and <em>classic</em> ballots
						</li>
						<li>If the fast ballot doesn't succeed, we fall back to the normal algorithm automatically</li>
					</ul>
				</section>
				<section>
					<h3>Fast Paxos (No collisions)</h3>
					<img class="plain" style="width:50%" src="images/fast-paxos.png"></img>
				</section>
				<section>
					<h3>Considerations</h3>
					<ul>
						<li>Quorum Size changes</li>
						<li>Either:
							<ul>
								<li>(2N/3) + 1 for both fast and classic quorums</li>
								<li>(f/2) + 1 for classic and (3N/4) for fast</li>
							</ul>
						</li>
						<li>Many collisions negate benefit</li>
						<li>WAN? (client proposes to all acceptors)</li>
					</ul>
				</section>

				<section>
					<h3>Generalized Paxos -- Reducing Collision Chances</h3>
					<ul>
						<li>Instead of ordering commands, order c-structs (collections of non-conflicting commands)</li>
						<li>Collisions only occur if two commands conflict with each other</li>
					</ul>
				</section>

				<section>
					<h3>Example Command Conflicts</h3>
					<table>
						<thead>
							<tr>
								<th></th>
								<th>read(x)</th>
								<th>inc(x)</th>
								<th>write(x,y)</th>
							</tr>
						</thead>
						<tr>
							<th>read(x)</th>
							<td></td>
							<td>x</td>
							<td>x</td>
						</tr>
						<tr>
							<th>read(y)</th>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<th>inc(x)</th>
							<td>x</td>
							<td></td>
							<td>x</td>
						</tr>
						<tr>
							<th>inc(y)</th>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<th>write(x,y)</th>
							<td>x</td>
							<td>x</td>
							<td></td>
						</tr>
						<tr>
							<th>write(x,z)</th>
							<td>x</td>
							<td>x</td>
							<td>x</td>
						</tr>
						<tr>
							<th>write(a,b)</th>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</table>
				</section>
			</section>

			<section>
				<section>
					<h3>Leader bottlenecks</h3>
				</section>
				<section>
					<h3>Potential Solutions</h3>
					<ul>
						<li>Leader leases</li>
						<li>Mencius (Round-robin leaders)</li>
						<li>EPaxos (Command Leaders)</li>
					</ul>
				</section>
				<section>
					<h3>Leader Leases</h3>
					<ul>
						<li style="color:#ff665b">Does not solve bottleneck</li>
						<li style="">Allows leader-related work to be somewhat distributed</li>
					</ul>
					<hr>
					<h3>Many Paxos groups</h3>
					<ul>
						<li>Creating many different Paxos instances can help reduce the problem of leader bottlenecks significantly.</li>
					</ul>
				</section>

				<section>
					<h3>Mencius</h3>
					<ul>
						<li><em>Idea: Multiple (possibly every) node is a leader,
								arranged in a logical ring</em></li>
						<li>Terminology: Mencius is Coordinated Paxos + Optimizations</li>
					</ul>
				</section>
				<section>
					<img src="images/mencius.png">
					<p>[4]</p>
				</section>
				<section>
					<h3>Mencius: Observations</h3>
					<ul>
						<li>Failures are more expensive (a failure always/usually means a leader failed)</li>
						<li>Requires failure detectors for progress</li>
						<li>Round-Robin nature may introduce delays (unbalanced requests)</li>
					</ul>
				</section>
				<section>
					<h3>Egalitarian Paxos</h3>
					<ul>
						<li>No long-lived leaders--instead, command leaders=process who is sent request from client</li>
						<li>Ordering of commands based on dependency tree built by analyzing conflicts</li>
						<li>Fast Path (all nodes in fast quorum agree on deps) and slow path (used in the case of conflicts</li>
						<li>Each node is responsible for ordering (all) messages iteself based on constructed dependency tree</li>
						<li>Combines ideas and extends ideas from Generalized Paxos and Mencius</li>
					</ul>
				</section>
			</section>

			<section>
				<h3>Bibliography</h3>
				<pre><code>
[1] L. Lamport and M. Massa, “Cheap paxos,” Florence, Italy,
June 2004. [Online]. Available: https://www.microsoft.com/
en-us/research/publication/cheap-paxos/
[2] L. Lamport, “Fast paxos,” Distributed Computing, vol. 19,
pp. 79–103, October 2006. [Online]. Available: https:
//www.microsoft.com/en-us/research/publication/fast-paxos/
[3] L. Lamport, “Generalized consensus and paxos,” March 2005
[Online].  Available: https://www.microsoft.com/en-us/
research/publication/generalized-consensus-and-paxos/
[4] Y. Mao, F. P. Junqueira, and K. Marzullo, “Mencius:
Building efficient replicated state machines for wans,”
in Proceedings of the 8th USENIX Conference
on Operating Systems Design and Implementation,
ser. OSDI’08.
Berkeley, CA, USA: USENIX
Association, 2008, pp. 369–384. [Online]. Available:
http://dl.acm.org/citation.cfm?id=1855741.1855767
[5] I. Moraru, D. G. Andersen, and M. Kaminsky, “There is
more consensus in egalitarian parliaments,” in Proceedings
of the Twenty-Fourth ACM Symposium on Operating
Systems Principles, ser. SOSP ’13. New York, NY,
USA: ACM, 2013, pp. 358–372. [Online]. Available:
http://doi.acm.org/10.1145/2517349.2517350
				</code></pre>
			</section>

		</div>
	</div>

	<script src="lib/js/head.min.js"></script>
	<script src="js/reveal.js"></script>

	<script>
		// More info about config & dependencies:
		// - https://github.com/hakimel/reveal.js#configuration
		// - https://github.com/hakimel/reveal.js#dependencies
		Reveal.initialize({
			dependencies: [{
					src: 'plugin/markdown/marked.js'
				},
				{
					src: 'plugin/markdown/markdown.js'
				},
				{
					src: 'plugin/notes/notes.js',
					async: true
				},
				{
					src: 'plugin/highlight/highlight.js',
					async: true,
					callback: function() {
						hljs.initHighlightingOnLoad();
					}
				},
				{
					src: 'node_modules/reveald3/reveald3.js'
				}

			]
		});
	</script>
</body>

</html>
